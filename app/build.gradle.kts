plugins {
    id("org.jetbrains.kotlin.jvm") version "1.7.10"
    kotlin("plugin.serialization") version "1.7.10"

    // this plugin is used for my own purpose of having an API key in my code
    id("com.github.gmazzo.buildconfig") version "3.1.0"
    application
}

group = "org.example"
version = "unspecified"

repositories {
    mavenCentral()
}

dependencies {
    implementation(project(":lib"))

    implementation("io.ktor:ktor-client-content-negotiation:2.0.2")
    implementation("io.ktor:ktor-serialization-kotlinx-json:2.0.2")
    implementation("io.ktor:ktor-client-resources:2.0.2")

    implementation("ch.qos.logback:logback-classic:1.2.11")
    implementation("io.ktor:ktor-client-core-jvm:2.0.3")
    implementation("io.ktor:ktor-client-cio-jvm:2.0.3")
    implementation("io.ktor:ktor-client-logging-jvm:2.0.3")

    testImplementation("org.junit.jupiter:junit-jupiter-api:5.8.1")
    testRuntimeOnly("org.junit.jupiter:junit-jupiter-engine:5.8.1")
}

tasks.getByName<Test>("test") {
    useJUnitPlatform()
}

application {
    mainClass.set("com.gitlab.djsushi123.kram.MainKt")
}

buildConfig {
    buildConfigField(
        "String",
        "TIMTIN_BOT_TOKEN",
        '"' + project.findProperty("kram.TIMTIN_BOT_TOKEN").toString() + '"'
    )
    buildConfigField(
        "String",
        "SLOVAK_ERECEIPT_BOT_TOKEN",
        '"' + project.findProperty("kram.SLOVAK_ERECEIPT_BOT_TOKEN").toString() + '"'
    )
}