package com.gitlab.djsushi123.kram

import kotlinx.serialization.Serializable
import java.text.DecimalFormat

@Serializable
data class ItemDTO(
    val name: String,
    val itemType: String, // TODO add enum for this
    val quantity: Double,
    val vatRate: Double,
    val price: Double
) {
    override fun toString(): String {
        val decimalFormat = DecimalFormat("#.#")
        return "${decimalFormat.format(quantity)}x ${name.trim()} ; ${price}EUR"
    }
}
