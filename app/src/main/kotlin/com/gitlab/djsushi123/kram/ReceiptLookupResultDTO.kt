package com.gitlab.djsushi123.kram

import kotlinx.serialization.Serializable
import kotlinx.serialization.json.JsonObject

@Serializable
data class ReceiptLookupResultDTO(
    val returnValue: Int,
    val receipt: ReceiptDTO?,
    val searchIdentification: JsonObject
)