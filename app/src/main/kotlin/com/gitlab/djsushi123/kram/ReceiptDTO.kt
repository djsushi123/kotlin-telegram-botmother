package com.gitlab.djsushi123.kram

import kotlinx.serialization.Serializable
import kotlinx.serialization.json.JsonObject

@Serializable
data class ReceiptDTO(
    val receiptId: String,
    val ico: String,
    val cashRegisterCode: String,
    val issueDate: String,
    val createDate: String,
    val customerId: String?,
    val dic: String,
    val icDph: String,
    val invoiceNumber: String?,
    val okp: String,
    val paragon: Boolean,
    val paragonNumber: String?,
    val pkp: String,
    val receiptNumber: Int,
    val type: String, // TODO create type enum
    val taxBaseBasic: Double,
    val taxBaseReduced: Double,
    val totalPrice: Double,
    val freeTaxAmount: Double?,
    val vatAmountBasic: Double,
    val vatAmountReduced: Double,
    val vatRateBasic: Double,
    val vatRateReduced: Double,
    val items: List<ItemDTO>,
    val organization: JsonObject,
    val unit: JsonObject,
    val exemption: Boolean
)