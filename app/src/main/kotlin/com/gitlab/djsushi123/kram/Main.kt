package com.gitlab.djsushi123.kram

import com.gitlab.djsushi123.kram.event.MessageReceiveEvent
import com.gitlab.djsushi123.kram.event.StickerMessageReceiveEvent
import com.gitlab.djsushi123.kram.event.TextMessageReceiveEvent
import io.ktor.client.*
import io.ktor.client.call.*
import io.ktor.client.engine.cio.*
import io.ktor.client.plugins.contentnegotiation.*
import io.ktor.client.plugins.logging.*
import io.ktor.client.request.*
import io.ktor.http.*
import io.ktor.serialization.kotlinx.json.*
import kotlinx.serialization.json.JsonObject
import kotlinx.serialization.json.JsonPrimitive
import org.example.app.BuildConfig

fun log(msg: Any?) = println("[${Thread.currentThread().name}] $msg")

suspend fun main() {
    val client = HttpClient(CIO) {
        install(ContentNegotiation) {
            json()
        }
        install(Logging) {
            level = LogLevel.NONE
        }
    }

    val bot = kramBot(token = BuildConfig.SLOVAK_ERECEIPT_BOT_TOKEN)

    bot.on<MessageReceiveEvent> {
        println(message.messageId)
    }

    bot.on<StickerMessageReceiveEvent> {
        if (message.sticker.fileUniqueId == "AgAD-gADMNSdEQ") bot.sendMessage(
            chatId = message.chat.id,
            text = "fuck you",
            replyToMessageId = message.messageId
        )
    }

    bot.on<TextMessageReceiveEvent> {
        if (!message.text.startsWith("O-")) return@on

        val response = client.post("https://ekasa.financnasprava.sk/mdu/api/v1/opd/receipt/find") {
            contentType(ContentType.Application.Json)
            setBody(JsonObject(mapOf("receiptId" to JsonPrimitive(message.text))))
        }
        val receipt = response.body<ReceiptLookupResultDTO>().receipt

        if (receipt == null) {
            bot.sendMessage(
                chatId = message.chat.id,
                text = "Did not find your receipt!"
            )
        } else {
            val text = buildString {
                receipt.items.forEach {
                    append(it.toString() + "\n")
                }
            }
            bot.sendMessage(
                chatId = message.chat.id,
                text = "You bought:\n$text"
            )
        }
    }

    bot.start()
}