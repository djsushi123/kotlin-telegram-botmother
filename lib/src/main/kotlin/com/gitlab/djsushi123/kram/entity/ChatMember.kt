package com.gitlab.djsushi123.kram.entity

data class ChatMember(
    val status: String,
    val user: User,
    val isAnonymous: Boolean? = null,
    val customTitle: String? = null,
    val canBeEdited: Boolean? = null,
    val canManageChat: Boolean? = null,
    val canDeleteMessages: Boolean? = null,
    val canManageVideoChats: Boolean? = null,
    val canRestrictMembers: Boolean? = null,
    val canPromoteMembers: Boolean? = null,
    val canChangeInfo: Boolean? = null,
    val canInviteUsers: Boolean? = null,
    val canPostMessages: Boolean? = null,
    val canEditMessages: Boolean? = null,
    val canPinMessages: Boolean? = null,
    val canSendMediaMessages: Boolean? = null,
    val canSendPolls: Boolean? = null,
    val canSendOtherMessages: Boolean? = null,
    val canAddWebPagePreviews: Boolean? = null,
    val untilDate: Int? = null
)