package com.gitlab.djsushi123.kram.entity

data class WebAppData(
    val data: String,
    val buttonText: String
)