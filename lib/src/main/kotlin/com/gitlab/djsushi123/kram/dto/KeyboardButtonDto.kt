package com.gitlab.djsushi123.kram.dto

import com.gitlab.djsushi123.kram.entity.KeyboardButton
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
internal data class KeyboardButtonDto(
    @SerialName("text") val text: String,
    @SerialName("request_contact") val requestContact: Boolean? = null,
    @SerialName("request_location") val requestLocation: Boolean? = null,
    @SerialName("request_poll") val requestPoll: KeyboardButtonPollTypeDto? = null,
    @SerialName("web_app") val webApp: WebAppInfoDto? = null
)

internal fun KeyboardButtonDto.toEntity(): KeyboardButton {
    return KeyboardButton(
        text = text,
        requestContact = requestContact,
        requestLocation = requestLocation,
        requestPoll = requestPoll?.toEntity(),
        webApp = webApp?.toEntity()
    )
}

internal fun KeyboardButton.toDto(): KeyboardButtonDto {
    return KeyboardButtonDto(
        text = text,
        requestContact = requestContact,
        requestLocation = requestLocation,
        requestPoll = requestPoll?.toDto(),
        webApp = webApp?.toDto()
    )
}