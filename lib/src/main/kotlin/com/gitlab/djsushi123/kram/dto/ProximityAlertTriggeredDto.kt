package com.gitlab.djsushi123.kram.dto

import com.gitlab.djsushi123.kram.entity.ProximityAlertTriggered
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
internal data class ProximityAlertTriggeredDto(
    @SerialName("traveler") val traveler: UserDto,
    @SerialName("watcher") val watcher: UserDto,
    @SerialName("distance") val distance: Int,
)

internal fun ProximityAlertTriggeredDto.toEntity(): ProximityAlertTriggered {
    return ProximityAlertTriggered(
        traveler = traveler.toEntity(),
        watcher = watcher.toEntity(),
        distance = distance
    )
}

internal fun ProximityAlertTriggered.toDto(): ProximityAlertTriggeredDto {
    return ProximityAlertTriggeredDto(
        traveler = traveler.toDto(),
        watcher = watcher.toDto(),
        distance = distance
    )
}