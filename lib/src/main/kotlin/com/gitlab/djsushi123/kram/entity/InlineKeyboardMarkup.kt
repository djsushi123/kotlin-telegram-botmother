package com.gitlab.djsushi123.kram.entity

data class InlineKeyboardMarkup(
    val inlineKeyboard: List<List<InlineKeyboardButton>>
) : ReplyMarkup