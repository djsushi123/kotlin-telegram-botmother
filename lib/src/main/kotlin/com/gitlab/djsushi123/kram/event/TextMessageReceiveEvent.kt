package com.gitlab.djsushi123.kram.event

import com.gitlab.djsushi123.kram.KramBot
import com.gitlab.djsushi123.kram.entity.message.Message
import com.gitlab.djsushi123.kram.entity.message.TextMessage
import kotlinx.coroutines.CoroutineScope

class TextMessageReceiveEvent(
    private val bot: KramBot,
    val message: TextMessage,
    private val coroutineScope: CoroutineScope = kramCoroutineScope(bot)
) : Event, CoroutineScope by coroutineScope