package com.gitlab.djsushi123.kram

import kotlinx.serialization.json.Json

object Config {

    val json = Json {
        prettyPrint = true
    }

}