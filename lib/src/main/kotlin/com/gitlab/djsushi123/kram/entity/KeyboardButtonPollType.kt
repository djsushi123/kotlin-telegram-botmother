package com.gitlab.djsushi123.kram.entity

data class KeyboardButtonPollType(
    val type: String? = null
)