package com.gitlab.djsushi123.kram.dto

import com.gitlab.djsushi123.kram.entity.ParseMode
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
internal enum class ParseModeDto {
    @SerialName("MarkdownV2") MARKDOWN_V2,
    @SerialName("HTML") HTML,
    @SerialName("Markdown") MARKDOWN_LEGACY
}

internal fun ParseModeDto.toEntity(): ParseMode {
    return when (this) {
        ParseModeDto.MARKDOWN_V2 -> ParseMode.MARKDOWN_V2
        ParseModeDto.HTML -> ParseMode.HTML
        ParseModeDto.MARKDOWN_LEGACY -> ParseMode.MARKDOWN_LEGACY
    }
}

internal fun ParseMode.toDto(): ParseModeDto {
    return when (this) {
        ParseMode.MARKDOWN_V2 -> ParseModeDto.MARKDOWN_V2
        ParseMode.HTML -> ParseModeDto.HTML
        ParseMode.MARKDOWN_LEGACY -> ParseModeDto.MARKDOWN_LEGACY
    }
}