package com.gitlab.djsushi123.kram.dto

import com.gitlab.djsushi123.kram.entity.File
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
internal data class FileDto(
    @SerialName("file_id") val fileId: String,
    @SerialName("file_unique_id") val fileUniqueId: String,
    @SerialName("file_name") val fileName: String? = null,
    @SerialName("file_size") val fileSize: Long? = null
)

internal fun FileDto.toEntity(): File {
    return File(
        fileId = fileId,
        fileUniqueId = fileUniqueId,
        fileName = fileName,
        fileSize = fileSize
    )
}

internal fun File.toDto(): FileDto {
    return FileDto(
        fileId = fileId,
        fileUniqueId = fileUniqueId,
        fileName = fileName,
        fileSize = fileSize
    )
}