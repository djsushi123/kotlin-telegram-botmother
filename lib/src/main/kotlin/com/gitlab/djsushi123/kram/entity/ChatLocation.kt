package com.gitlab.djsushi123.kram.entity

data class ChatLocation(
    val location: Location,
    val address: String
)