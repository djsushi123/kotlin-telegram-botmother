package com.gitlab.djsushi123.kram.dto

import com.gitlab.djsushi123.kram.entity.Poll
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
internal data class PollDto(
    @SerialName("id") val id: String,
    @SerialName("question") val question: String,
    @SerialName("options") val options: List<PollOptionDto>,
    @SerialName("total_voter_count") val totalVoterCount: Int,
    @SerialName("is_closed") val isClosed: Boolean,
    @SerialName("is_anonymous") val isAnonymous: Boolean,
    @SerialName("type") val type: String,
    @SerialName("allows_multiple_answers") val allowsMultipleAnswers: Boolean,
    @SerialName("correct_option_id") val correctOptionId: Int? = null,
    @SerialName("explanation") val explanation: String? = null,
    @SerialName("explanation_entities") val explanationEntities: List<MessageEntityDto>? = null,
    @SerialName("open_period") val openPeriod: Int? = null,
    @SerialName("close_gate") val closeGate: Int? = null
)

internal fun PollDto.toEntity(): Poll {
    return Poll(
        id = id,
        question = question,
        options = options.map { it.toEntity() },
        totalVoterCount = totalVoterCount,
        isClosed = isClosed,
        isAnonymous = isAnonymous,
        type = type,
        allowsMultipleAnswers = allowsMultipleAnswers,
        correctOptionId = correctOptionId,
        explanation = explanation,
        explanationEntities = explanationEntities?.map { it.toEntity() },
        openPeriod = openPeriod,
        closeGate = closeGate
    )
}

internal fun Poll.toDto(): PollDto {
    return PollDto(
        id = id,
        question = question,
        options = options.map { it.toDto() },
        totalVoterCount = totalVoterCount,
        isClosed = isClosed,
        isAnonymous = isAnonymous,
        type = type,
        allowsMultipleAnswers = allowsMultipleAnswers,
        correctOptionId = correctOptionId,
        explanation = explanation,
        explanationEntities = explanationEntities?.map { it.toDto() },
        openPeriod = openPeriod,
        closeGate = closeGate
    )
}