package com.gitlab.djsushi123.kram.entity

data class EncryptedCredentials(
    val data: String,
    val hash: String,
    val secret: String
)