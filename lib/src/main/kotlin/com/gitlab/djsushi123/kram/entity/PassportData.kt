package com.gitlab.djsushi123.kram.entity

data class PassportData(
    val data: EncryptedPassportElement,
    val credentials: EncryptedCredentials
)