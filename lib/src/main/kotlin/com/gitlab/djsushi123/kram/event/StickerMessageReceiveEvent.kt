package com.gitlab.djsushi123.kram.event

import com.gitlab.djsushi123.kram.KramBot
import com.gitlab.djsushi123.kram.entity.message.Message
import com.gitlab.djsushi123.kram.entity.message.StickerMessage
import kotlinx.coroutines.CoroutineScope

class StickerMessageReceiveEvent(
    private val bot: KramBot,
    val message: StickerMessage,
    private val coroutineScope: CoroutineScope = kramCoroutineScope(bot)
) : Event, CoroutineScope by coroutineScope