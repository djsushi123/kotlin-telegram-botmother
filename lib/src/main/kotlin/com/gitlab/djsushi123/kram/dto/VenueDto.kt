package com.gitlab.djsushi123.kram.dto

import com.gitlab.djsushi123.kram.entity.Venue
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
internal data class VenueDto(
    @SerialName("location") val location: LocationDto,
    @SerialName("title") val title: String,
    @SerialName("address") val address: String,
    @SerialName("foursquare_id") val foursquareId: String? = null,
    @SerialName("foursquare_type") val foursquareType: String? = null,
    @SerialName("google_place_id") val googlePlaceId: String? = null,
    @SerialName("google_place_type") val googlePlaceType: String? = null
)

internal fun VenueDto.toEntity(): Venue {
    return Venue(
        location.toEntity(),
        title,
        address,
        foursquareId,
        foursquareType,
        googlePlaceId,
        googlePlaceType
    )
}

internal fun Venue.toDto(): VenueDto {
    return VenueDto(
        location.toDto(),
        title,
        address,
        foursquareId,
        foursquareType,
        googlePlaceId,
        googlePlaceType
    )
}