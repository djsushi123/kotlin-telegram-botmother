package com.gitlab.djsushi123.kram.entity

data class ProximityAlertTriggered(
    val traveler: User,
    val watcher: User,
    val distance: Int,
)