package com.gitlab.djsushi123.kram.entity

data class ChatJoinRequest(
    val chat: Chat,
    val from: User,
    val date: Int,
    val bio: String,
    val inviteLink: ChatInviteLink? = null
)