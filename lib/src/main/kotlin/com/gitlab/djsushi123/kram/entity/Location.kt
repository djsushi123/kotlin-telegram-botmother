package com.gitlab.djsushi123.kram.entity

data class Location(
    val longitude: Float,
    val latitude: Float,
    val horizontalAccuracy: Float? = null,
    val livePeriod: Int? = null,
    val heading: Int? = null,
    val proximityAlertRadius: Int? = null
)