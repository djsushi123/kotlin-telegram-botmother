package com.gitlab.djsushi123.kram.dto

import com.gitlab.djsushi123.kram.entity.Game
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
internal data class GameDto(
    @SerialName("title") val title: String,
    @SerialName("description") val description: String,
    @SerialName("photo") val photo: List<PhotoSizeDto>,
    @SerialName("text") val text: String? = null,
    @SerialName("text_entities") val textEntities: List<MessageEntityDto>? = null,
    @SerialName("animation") val animation: AnimationDto? = null
)

internal fun GameDto.toEntity(): Game {
    return Game(
        title = title,
        description = description,
        photo = photo.map { it.toEntity() },
        text = text,
        textEntities?.map { it.toEntity() },
        animation?.toEntity()
    )
}

internal fun Game.toDto(): GameDto {
    return GameDto(
        title = title,
        description = description,
        photo = photo.map { it.toDto() },
        text = text,
        textEntities?.map { it.toDto() },
        animation?.toDto()
    )
}