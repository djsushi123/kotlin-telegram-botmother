package com.gitlab.djsushi123.kram.entity

data class MaskPosition(
    val point: String,
    val xShift: Float,
    val yShift: Float,
    val scale: Float
)