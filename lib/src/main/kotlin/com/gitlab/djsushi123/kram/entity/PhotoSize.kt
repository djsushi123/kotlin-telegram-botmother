package com.gitlab.djsushi123.kram.entity

data class PhotoSize(
    val fileId: String,
    val fileUniqueId: String,
    val width: Int,
    val height: Int,
    val fileSize: Int? = null
)