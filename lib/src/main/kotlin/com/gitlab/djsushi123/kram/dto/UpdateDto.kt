package com.gitlab.djsushi123.kram.dto

import com.gitlab.djsushi123.kram.entity.Update
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
internal data class UpdateDto(
    @SerialName("update_id") val updateId: Int,
    @SerialName("message") val message: MessageDto? = null,
    @SerialName("edited_message") val editedMessage: MessageDto? = null,
    @SerialName("channel_post") val channelPost: MessageDto? = null,
    @SerialName("edited_channel_post") val editedChannelPost: MessageDto? = null,
    @SerialName("inline_query") val inlineQuery: InlineQueryDto? = null,
    @SerialName("chosen_inline_result") val chosenInlineResult: ChosenInlineResultDto? = null,
    @SerialName("callback_query") val callbackQuery: CallbackQueryDto? = null,
    @SerialName("shipping_query") val shippingQuery: ShippingQueryDto? = null,
    @SerialName("pre_checkout_query") val preCheckoutQuery: PreCheckoutQueryDto? = null,
    @SerialName("poll") val poll: PollDto? = null,
    @SerialName("poll_answer") val pollAnswer: PollAnswerDto? = null,
    @SerialName("my_chat_member") val myChatMember: ChatMemberUpdatedDto? = null,
    @SerialName("chat_member") val chatMember: ChatMemberUpdatedDto? = null,
    @SerialName("chat_join_request") val chatJoinRequest: ChatJoinRequestDto? = null
)

internal fun UpdateDto.toEntity(): Update {
    return Update(
        updateId = updateId,
        message = message?.toEntity(),
        editedMessage = editedMessage?.toEntity(),
        channelPost = channelPost?.toEntity(),
        editedChannelPost = editedChannelPost?.toEntity(),
        inlineQuery = inlineQuery?.toEntity(),
        chosenInlineResult = chosenInlineResult?.toEntity(),
        callbackQuery = callbackQuery?.toEntity(),
        shippingQuery = shippingQuery?.toEntity(),
        preCheckoutQuery = preCheckoutQuery?.toEntity(),
        poll = poll?.toEntity(),
        pollAnswer = pollAnswer?.toEntity(),
        myChatMember = myChatMember?.toEntity(),
        chatMember = chatMember?.toEntity(),
        chatJoinRequest = chatJoinRequest?.toEntity()
    )
}

internal fun Update.toDto(): UpdateDto {
    return UpdateDto(
        updateId = updateId,
        message = message?.toDto(),
        editedMessage = editedMessage?.toDto(),
        channelPost = channelPost?.toDto(),
        editedChannelPost = editedChannelPost?.toDto(),
        inlineQuery = inlineQuery?.toDto(),
        chosenInlineResult = chosenInlineResult?.toDto(),
        callbackQuery = callbackQuery?.toDto(),
        shippingQuery = shippingQuery?.toDto(),
        preCheckoutQuery = preCheckoutQuery?.toDto(),
        poll = poll?.toDto(),
        pollAnswer = pollAnswer?.toDto(),
        myChatMember = myChatMember?.toDto(),
        chatMember = chatMember?.toDto(),
        chatJoinRequest = chatJoinRequest?.toDto()
    )
}