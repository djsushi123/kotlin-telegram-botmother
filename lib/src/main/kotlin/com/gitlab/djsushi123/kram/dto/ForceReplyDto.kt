package com.gitlab.djsushi123.kram.dto

import com.gitlab.djsushi123.kram.entity.ForceReply
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class ForceReplyDto(
    @SerialName("force_reply") val forceReply: Boolean,
    @SerialName("input_field_placeholder") val inputFieldPlaceholder: String? = null,
    @SerialName("selective") val selective: Boolean? = null
)

internal fun ForceReplyDto.toEntity(): ForceReply {
    return ForceReply(
        forceReply = forceReply,
        inputFieldPlaceholder = inputFieldPlaceholder,
        selective = selective
    )
}

internal fun ForceReply.toDto(): ForceReplyDto {
    return ForceReplyDto(
        forceReply = forceReply,
        inputFieldPlaceholder = inputFieldPlaceholder,
        selective = selective
    )
}