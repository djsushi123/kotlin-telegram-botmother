package com.gitlab.djsushi123.kram.entity

data class WebAppInfo(
    val url: String
)