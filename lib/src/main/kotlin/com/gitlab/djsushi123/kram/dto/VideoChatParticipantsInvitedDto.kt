package com.gitlab.djsushi123.kram.dto

import com.gitlab.djsushi123.kram.entity.VideoChatParticipantsInvited
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
internal data class VideoChatParticipantsInvitedDto(
    @SerialName("users") val users: List<UserDto>
)

internal fun VideoChatParticipantsInvitedDto.toEntity(): VideoChatParticipantsInvited {
    return VideoChatParticipantsInvited(
        users = users.map { it.toEntity() }
    )
}

internal fun VideoChatParticipantsInvited.toDto(): VideoChatParticipantsInvitedDto {
    return VideoChatParticipantsInvitedDto(
        users = users.map { it.toDto() }
    )
}