package com.gitlab.djsushi123.kram.entity

data class PassportFile(
    val fileId: String,
    val fileUniqueId: String,
    val fileSize: Long,
    val fileDate: Int
)