package com.gitlab.djsushi123.kram.dto

import com.gitlab.djsushi123.kram.entity.CallbackQuery
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
internal data class CallbackQueryDto(
    @SerialName("id") val id: String,
    @SerialName("from") val from: UserDto,
    @SerialName("message") val message: MessageDto? = null,
    @SerialName("inline_message_id") val inlineMessageId: String? = null,
    @SerialName("chat_instance") val chatInstance: String,
    @SerialName("data") val data: String? = null,
    @SerialName("game_short_name") val gameShortName: String? = null
)

internal fun CallbackQueryDto.toEntity(): CallbackQuery {
    return CallbackQuery(
        id = id,
        from = from.toEntity(),
        message = message?.toEntity(),
        inlineMessageId = inlineMessageId,
        chatInstance = chatInstance,
        data = data,
        gameShortName = gameShortName
    )
}

internal fun CallbackQuery.toDto(): CallbackQueryDto {
    return CallbackQueryDto(
        id = id,
        from = from.toDto(),
        message = message?.toDto(),
        inlineMessageId = inlineMessageId,
        chatInstance = chatInstance,
        data = data,
        gameShortName = gameShortName
    )
}