package com.gitlab.djsushi123.kram.entity

data class PollAnswer(
    val pollId: String,
    val user: User,
    val optionIds: List<Int>
)