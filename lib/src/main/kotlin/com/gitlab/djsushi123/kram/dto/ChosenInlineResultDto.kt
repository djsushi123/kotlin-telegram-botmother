package com.gitlab.djsushi123.kram.dto

import com.gitlab.djsushi123.kram.entity.ChosenInlineResult
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
internal data class ChosenInlineResultDto(
    @SerialName("result_id") val resultId: String,
    @SerialName("from") val from: UserDto,
    @SerialName("location") val location: LocationDto? = null,
    @SerialName("inline_message_id") val inlineMessageId: String? = null,
    @SerialName("query") val query: String
)

internal fun ChosenInlineResultDto.toEntity(): ChosenInlineResult {
    return ChosenInlineResult(
        resultId = resultId,
        from = from.toEntity(),
        location = location?.toEntity(),
        inlineMessageId = inlineMessageId,
        query = query
    )
}

internal fun ChosenInlineResult.toDto(): ChosenInlineResultDto {
    return ChosenInlineResultDto(
        resultId = resultId,
        from = from.toDto(),
        location = location?.toDto(),
        inlineMessageId = inlineMessageId,
        query = query
    )
}