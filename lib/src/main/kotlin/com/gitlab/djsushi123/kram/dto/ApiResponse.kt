package com.gitlab.djsushi123.kram.dto

import kotlinx.serialization.KSerializer
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable
import kotlinx.serialization.descriptors.SerialDescriptor
import kotlinx.serialization.descriptors.buildClassSerialDescriptor
import kotlinx.serialization.encoding.Decoder
import kotlinx.serialization.encoding.Encoder
import kotlinx.serialization.json.JsonDecoder
import kotlinx.serialization.json.jsonObject
import kotlinx.serialization.json.jsonPrimitive

@Serializable(with = ApiResponseSerializer::class)
internal sealed class ApiResponse<out T> {

    @SerialName("ok")
    abstract val ok: Boolean

    @Serializable
    class Ok<out T>(
        override val ok: Boolean,
        @SerialName("result") val result: T
    ) : ApiResponse<T>()

    @Serializable
    class Error(
        override val ok: Boolean,
        @SerialName("error_code") val errorCode: Int,
        @SerialName("description") val description: String
    ) : ApiResponse<Nothing>()
}

internal class ApiResponseSerializer<T>(private val tSerializer: KSerializer<T>) : KSerializer<ApiResponse<T>> {
    override val descriptor: SerialDescriptor = buildClassSerialDescriptor("ApiResponse", tSerializer.descriptor)

    override fun serialize(encoder: Encoder, value: ApiResponse<T>) {
        when (value) {
            is ApiResponse.Ok -> encoder.encodeSerializableValue(
                serializer = ApiResponse.Ok.serializer(tSerializer),
                value = ApiResponse.Ok(true, value.result)
            )
            is ApiResponse.Error -> encoder.encodeSerializableValue(
                serializer = ApiResponse.Error.serializer(),
                value = ApiResponse.Error(false, value.errorCode, value.description)
            )
        }
    }

    override fun deserialize(decoder: Decoder): ApiResponse<T> {
        check(decoder is JsonDecoder) { "This serializer can be used only with Json format." }
        val element = decoder.decodeJsonElement()
        return when (element.jsonObject["ok"]?.jsonPrimitive?.content) {
            "true" -> decoder.json.decodeFromJsonElement(ApiResponse.Ok.serializer(tSerializer), element)
            else -> decoder.json.decodeFromJsonElement(ApiResponse.Error.serializer(), element)
        }
    }
}