package com.gitlab.djsushi123.kram.dto

import com.gitlab.djsushi123.kram.entity.PassportData
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
internal data class PassportDataDto(
    @SerialName("data") val data: EncryptedPassportElementDto,
    @SerialName("credentials") val credentials: EncryptedCredentialsDto
)

internal fun PassportDataDto.toEntity(): PassportData {
    return PassportData(
        data = data.toEntity(),
        credentials = credentials.toEntity()
    )
}

internal fun PassportData.toDto(): PassportDataDto {
    return PassportDataDto(
        data = data.toDto(),
        credentials = credentials.toDto()
    )
}