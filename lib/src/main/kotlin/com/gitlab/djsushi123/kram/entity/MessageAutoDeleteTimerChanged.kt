package com.gitlab.djsushi123.kram.entity

data class MessageAutoDeleteTimerChanged(
    val messageAutoDeleteTime: Int
)