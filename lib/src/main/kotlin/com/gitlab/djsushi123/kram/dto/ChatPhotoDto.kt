package com.gitlab.djsushi123.kram.dto

import com.gitlab.djsushi123.kram.entity.ChatPhoto
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
internal data class ChatPhotoDto(
    @SerialName("small_file_id") val smallFileId: String,
    @SerialName("small_file_unique_id") val smallFileUniqueId: String,
    @SerialName("big_file_id") val bigFileId: String,
    @SerialName("big_file_unique_id") val bigFileUniqueId: String
)

internal fun ChatPhotoDto.toEntity(): ChatPhoto {
    return ChatPhoto(
        smallFileId = smallFileId,
        smallFileUniqueId = smallFileUniqueId,
        bigFileId = bigFileId,
        bigFileUniqueId = bigFileUniqueId
    )
}

internal fun ChatPhoto.toDto(): ChatPhotoDto {
    return ChatPhotoDto(
        smallFileId = smallFileId,
        smallFileUniqueId = smallFileUniqueId,
        bigFileId = bigFileId,
        bigFileUniqueId = bigFileUniqueId
    )
}