package com.gitlab.djsushi123.kram.dto

import com.gitlab.djsushi123.kram.entity.VideoChatEnded
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
internal data class VideoChatEndedDto(
    @SerialName("duration") val duration: Int
)

internal fun VideoChatEndedDto.toEntity(): VideoChatEnded {
    return VideoChatEnded(
        duration = duration
    )
}

internal fun VideoChatEnded.toDto(): VideoChatEndedDto {
    return VideoChatEndedDto(
        duration = duration
    )
}