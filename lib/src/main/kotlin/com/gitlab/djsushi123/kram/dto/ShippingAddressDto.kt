package com.gitlab.djsushi123.kram.dto

import com.gitlab.djsushi123.kram.entity.ShippingAddress
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
internal data class ShippingAddressDto(
    @SerialName("country_code") val countryCode: String,
    @SerialName("state") val state: String,
    @SerialName("city") val city: String,
    @SerialName("street_line1") val streetLine1: String,
    @SerialName("street_line2") val streetLine2: String,
    @SerialName("post_code") val postCode: String
)

internal fun ShippingAddressDto.toEntity(): ShippingAddress {
    return ShippingAddress(
        countryCode = countryCode,
        state = state,
        city = city,
        streetLine1 = streetLine1,
        streetLine2 = streetLine2,
        postCode = postCode
    )
}

internal fun ShippingAddress.toDto(): ShippingAddressDto {
    return ShippingAddressDto(
        countryCode = countryCode,
        state = state,
        city = city,
        streetLine1 = streetLine1,
        streetLine2 = streetLine2,
        postCode = postCode
    )
}