package com.gitlab.djsushi123.kram.dto

import com.gitlab.djsushi123.kram.entity.ChatJoinRequest
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
internal data class ChatJoinRequestDto(
    @SerialName("chat") val chat: ChatDto,
    @SerialName("from") val from: UserDto,
    @SerialName("date") val date: Int,
    @SerialName("bio") val bio: String,
    @SerialName("invite_link") val inviteLink: ChatInviteLinkDto? = null
)

internal fun ChatJoinRequestDto.toEntity(): ChatJoinRequest {
    return ChatJoinRequest(
        chat = chat.toEntity(),
        from = from.toEntity(),
        date = date,
        bio,
        inviteLink = inviteLink?.toEntity()
    )
}

internal fun ChatJoinRequest.toDto(): ChatJoinRequestDto {
    return ChatJoinRequestDto(
        chat = chat.toDto(),
        from = from.toDto(),
        date = date,
        bio,
        inviteLink = inviteLink?.toDto()
    )
}