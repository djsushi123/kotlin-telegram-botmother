package com.gitlab.djsushi123.kram.dto

import com.gitlab.djsushi123.kram.entity.VideoChatStarted
import kotlinx.serialization.Serializable

@Serializable
internal class VideoChatStartedDto

internal fun VideoChatStartedDto.toEntity(): VideoChatStarted {
    return VideoChatStarted()
}

internal fun VideoChatStarted.toDto(): VideoChatStartedDto {
    return VideoChatStartedDto()
}