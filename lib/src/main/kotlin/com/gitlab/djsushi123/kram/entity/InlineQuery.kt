package com.gitlab.djsushi123.kram.entity

data class InlineQuery(
    val id: String,
    val from: User,
    val query: String,
    val offset: String,
    val chatType: String? = null,
    val location: Location? = null
)