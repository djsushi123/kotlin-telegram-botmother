package com.gitlab.djsushi123.kram.dto

import com.gitlab.djsushi123.kram.entity.KeyboardButtonPollType
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
internal data class KeyboardButtonPollTypeDto(
    @SerialName("type") val type: String? = null
)

internal fun KeyboardButtonPollTypeDto.toEntity(): KeyboardButtonPollType {
    return KeyboardButtonPollType(
        type = type
    )
}

internal fun KeyboardButtonPollType.toDto(): KeyboardButtonPollTypeDto {
    return KeyboardButtonPollTypeDto(
        type = type
    )
}