package com.gitlab.djsushi123.kram.entity

data class PollOption(
    val text: String,
    val voterCount: Int
)