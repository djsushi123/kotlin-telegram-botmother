package com.gitlab.djsushi123.kram.dto

import com.gitlab.djsushi123.kram.entity.ReplyKeyboardMarkup
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
internal data class ReplyKeyboardMarkupDto(
    @SerialName("keyboard") val keyboard: List<List<KeyboardButtonDto>>,
    @SerialName("resize_keyboard") val resizeKeyboard: Boolean? = null,
    @SerialName("one_time_keyboard") val oneTimeKeyboard: Boolean? = null,
    @SerialName("input_field_placeholder") val inputFieldPlaceholder: String? = null,
    @SerialName("selective") val selective: Boolean? = null
)

internal fun ReplyKeyboardMarkupDto.toEntity(): ReplyKeyboardMarkup {
    return ReplyKeyboardMarkup(
        keyboard = keyboard.map { keyboardButtonList ->
            keyboardButtonList.map { it.toEntity() }
        },
        resizeKeyboard = resizeKeyboard,
        oneTimeKeyboard = oneTimeKeyboard,
        inputFieldPlaceholder = inputFieldPlaceholder,
        selective = selective
    )
}

internal fun ReplyKeyboardMarkup.toDto(): ReplyKeyboardMarkupDto {
    return ReplyKeyboardMarkupDto(
        keyboard = keyboard.map { keyboardButtonList ->
            keyboardButtonList.map { it.toDto() }
        },
        resizeKeyboard = resizeKeyboard,
        oneTimeKeyboard = oneTimeKeyboard,
        inputFieldPlaceholder = inputFieldPlaceholder,
        selective = selective
    )
}