package com.gitlab.djsushi123.kram.dto

import com.gitlab.djsushi123.kram.entity.WebAppInfo
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
internal data class WebAppInfoDto(
    @SerialName("url") val url: String
)

internal fun WebAppInfoDto.toEntity(): WebAppInfo {
    return WebAppInfo(
        url = url
    )
}

internal fun WebAppInfo.toDto(): WebAppInfoDto {
    return WebAppInfoDto(
        url = url
    )
}