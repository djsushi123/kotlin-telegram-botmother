package com.gitlab.djsushi123.kram.dto

import com.gitlab.djsushi123.kram.entity.ShippingQuery
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
internal data class ShippingQueryDto(
    @SerialName("id") val id: String,
    @SerialName("from") val from: UserDto,
    @SerialName("invoice_payload") val invoicePayload: String,
    @SerialName("shipping_address") val shippingAddress: ShippingAddressDto
)

internal fun ShippingQueryDto.toEntity(): ShippingQuery {
    return ShippingQuery(
        id = id,
        from = from.toEntity(),
        invoicePayload = invoicePayload,
        shippingAddress = shippingAddress.toEntity()
    )
}

internal fun ShippingQuery.toDto(): ShippingQueryDto {
    return ShippingQueryDto(
        id = id,
        from = from.toDto(),
        invoicePayload = invoicePayload,
        shippingAddress = shippingAddress.toDto()
    )
}