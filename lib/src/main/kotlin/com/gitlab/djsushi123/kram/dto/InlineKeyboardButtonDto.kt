package com.gitlab.djsushi123.kram.dto

import com.gitlab.djsushi123.kram.entity.InlineKeyboardButton
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
internal data class InlineKeyboardButtonDto(
    @SerialName("text") val text: String,
    @SerialName("url") val url: String? = null,
    @SerialName("callback_data") val callbackData: String? = null,
    @SerialName("web_app") val webApp: WebAppInfoDto? = null,
    @SerialName("login_url") val loginUrl: LoginUrlDto? = null,
    @SerialName("switch_inline_query") val switchInlineQuery: String? = null,
    @SerialName("switch_inline_query_current_chat") val switchInlineQueryCurrentChat: String? = null,
    @SerialName("callback_game") val callbackGame: CallbackGameDto? = null,
    @SerialName("pay") val pay: Boolean? = null
)

internal fun InlineKeyboardButtonDto.toEntity(): InlineKeyboardButton {
    return InlineKeyboardButton(
        text = text,
        url = url,
        callbackData = callbackData,
        webApp = webApp?.toEntity(),
        loginUrl = loginUrl?.toEntity(),
        switchInlineQuery = switchInlineQuery,
        switchInlineQueryCurrentChat = switchInlineQueryCurrentChat,
        callbackGame = callbackGame?.toEntity(),
        pay = pay
    )
}

internal fun InlineKeyboardButton.toDto(): InlineKeyboardButtonDto {
    return InlineKeyboardButtonDto(
        text = text,
        url = url,
        callbackData = callbackData,
        webApp = webApp?.toDto(),
        loginUrl = loginUrl?.toDto(),
        switchInlineQuery = switchInlineQuery,
        switchInlineQueryCurrentChat = switchInlineQueryCurrentChat,
        callbackGame = callbackGame?.toDto(),
        pay = pay
    )
}