package com.gitlab.djsushi123.kram.entity

data class VideoChatEnded(
    val duration: Int
)