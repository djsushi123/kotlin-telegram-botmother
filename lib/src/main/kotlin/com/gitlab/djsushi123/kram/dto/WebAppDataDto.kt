package com.gitlab.djsushi123.kram.dto

import com.gitlab.djsushi123.kram.entity.WebAppData
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
internal data class WebAppDataDto(
    @SerialName("data") val data: String,
    @SerialName("button_text") val buttonText: String
)

internal fun WebAppDataDto.toEntity(): WebAppData {
    return WebAppData(
        data = data,
        buttonText = buttonText
    )
}

internal fun WebAppData.toDto(): WebAppDataDto {
    return WebAppDataDto(
        data = data,
        buttonText = buttonText
    )
}