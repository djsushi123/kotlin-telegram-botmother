package com.gitlab.djsushi123.kram.dto

import com.gitlab.djsushi123.kram.entity.PollOption
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
internal data class PollOptionDto(
    @SerialName("text") val text: String,
    @SerialName("voter_count") val voterCount: Int
)

internal fun PollOptionDto.toEntity(): PollOption {
    return PollOption(
        text = text,
        voterCount = voterCount
    )
}

internal fun PollOption.toDto(): PollOptionDto {
    return PollOptionDto(
        text = text,
        voterCount = voterCount
    )
}