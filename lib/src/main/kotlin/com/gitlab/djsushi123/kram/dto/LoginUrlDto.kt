package com.gitlab.djsushi123.kram.dto

import com.gitlab.djsushi123.kram.entity.LoginUrl
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
internal data class LoginUrlDto(
    @SerialName("url") val url: String,
    @SerialName("forward_text") val forwardText: String? = null,
    @SerialName("bot_username") val botUsername: String? = null,
    @SerialName("request_write_access") val requestWriteAccess: Boolean? = null,
)

internal fun LoginUrlDto.toEntity(): LoginUrl {
    return LoginUrl(
        url = url,
        forwardText = forwardText,
        botUsername = botUsername,
        requestWriteAccess = requestWriteAccess
    )
}

internal fun LoginUrl.toDto(): LoginUrlDto {
    return LoginUrlDto(
        url = url,
        forwardText = forwardText,
        botUsername = botUsername,
        requestWriteAccess = requestWriteAccess
    )
}