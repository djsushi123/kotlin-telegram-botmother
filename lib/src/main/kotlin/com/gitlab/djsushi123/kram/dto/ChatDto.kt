package com.gitlab.djsushi123.kram.dto

import com.gitlab.djsushi123.kram.entity.Chat
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
internal data class ChatDto(
    @SerialName("id") val id: Int,
    @SerialName("type") val type: String,
    @SerialName("title") val title: String? = null,
    @SerialName("username") val username: String? = null,
    @SerialName("first_name") val firstName: String? = null,
    @SerialName("last_name") val lastName: String? = null,
    @SerialName("photo") val photo: ChatPhotoDto? = null,
    @SerialName("bio") val bio: String? = null,
    @SerialName("has_private_forwards") val hasPrivateForwards: Boolean? = null,
    @SerialName("join_to_send_messages") val joinToSendMessages: Boolean? = null,
    @SerialName("join_by_request") val joinByRequest: Boolean? = null,
    @SerialName("description") val description: String? = null,
    @SerialName("invite_link") val inviteLink: String? = null,
    @SerialName("pinned_message") val pinnedMessage: MessageDto? = null,
    @SerialName("permissions") val permissions: ChatPermissionsDto? = null,
    @SerialName("slow_mode_delay") val slowModeDelay: Int? = null,
    @SerialName("message_auto_delete_time") val messageAutoDeleteTime: Int? = null,
    @SerialName("has_protected_content") val hasProtectedContent: Boolean? = null,
    @SerialName("sticker_set_name") val stickerSetName: String? = null,
    @SerialName("can_set_sticker_set") val canSetStickerSet: Boolean? = null,
    @SerialName("linked_chat_id") val linkedChatId: Int? = null,
    @SerialName("location") val location: ChatLocationDto? = null
)

internal fun ChatDto.toEntity(): Chat {
    return Chat(
        id = id,
        type = type,
        title = title,
        username = username,
        firstName = firstName,
        lastName = lastName,
        photo = photo?.toEntity(),
        bio = bio,
        hasPrivateForwards = hasPrivateForwards,
        joinToSendMessages = joinToSendMessages,
        joinByRequest = joinByRequest,
        description = description,
        inviteLink = inviteLink,
        pinnedMessage = pinnedMessage?.toEntity(),
        permissions = permissions?.toEntity(),
        slowModeDelay = slowModeDelay,
        messageAutoDeleteTime = messageAutoDeleteTime,
        hasProtectedContent = hasProtectedContent,
        stickerSetName = stickerSetName,
        canSetStickerSet = canSetStickerSet,
        linkedChatId = linkedChatId,
        location = location?.toEntity()
    )
}

internal fun Chat.toDto(): ChatDto {
    return ChatDto(
        id = id,
        type = type,
        title = title,
        username = username,
        firstName = firstName,
        lastName = lastName,
        photo = photo?.toDto(),
        bio = bio,
        hasPrivateForwards = hasPrivateForwards,
        joinToSendMessages = joinToSendMessages,
        joinByRequest = joinByRequest,
        description = description,
        inviteLink = inviteLink,
        pinnedMessage = pinnedMessage?.toDto(),
        permissions = permissions?.toDto(),
        slowModeDelay = slowModeDelay,
        messageAutoDeleteTime = messageAutoDeleteTime,
        hasProtectedContent = hasProtectedContent,
        stickerSetName = stickerSetName,
        canSetStickerSet = canSetStickerSet,
        linkedChatId = linkedChatId,
        location = location?.toDto()
    )
}