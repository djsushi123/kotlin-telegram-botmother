package com.gitlab.djsushi123.kram.dto

import com.gitlab.djsushi123.kram.entity.EncryptedPassportElement
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
internal data class EncryptedPassportElementDto(
    @SerialName("type") val type: String,
    @SerialName("data") val data: String? = null,
    @SerialName("phone_number") val phoneNumber: String? = null,
    @SerialName("email") val email: String? = null,
    @SerialName("files") val files: List<PassportFileDto>? = null,
    @SerialName("front_side") val frontSize: PassportFileDto? = null,
    @SerialName("reverse_side") val reverseSide: PassportFileDto? = null,
    @SerialName("selfie") val selfie: PassportFileDto? = null,
    @SerialName("translation") val translation: List<PassportFileDto>? = null,
    @SerialName("hash") val hash: String
)

internal fun EncryptedPassportElementDto.toEntity(): EncryptedPassportElement {
    return EncryptedPassportElement(
        type = type,
        data = data,
        phoneNumber = phoneNumber,
        email = email,
        files = files?.map { it.toEntity() },
        frontSize = frontSize?.toEntity(),
        reverseSide = reverseSide?.toEntity(),
        selfie = selfie?.toEntity(),
        translation = translation?.map { it.toEntity() },
        hash = hash
    )
}

internal fun EncryptedPassportElement.toDto(): EncryptedPassportElementDto {
    return EncryptedPassportElementDto(
        type = type,
        data = data,
        phoneNumber = phoneNumber,
        email = email,
        files = files?.map { it.toDto() },
        frontSize = frontSize?.toDto(),
        reverseSide = reverseSide?.toDto(),
        selfie = selfie?.toDto(),
        translation = translation?.map { it.toDto() },
        hash = hash
    )
}