package com.gitlab.djsushi123.kram.dto

import com.gitlab.djsushi123.kram.entity.EncryptedCredentials
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
internal data class EncryptedCredentialsDto(
    @SerialName("data") val data: String,
    @SerialName("hash") val hash: String,
    @SerialName("secret") val secret: String
)

internal fun EncryptedCredentialsDto.toEntity(): EncryptedCredentials {
    return EncryptedCredentials(
        data = data,
        hash = hash,
        secret = secret
    )
}

internal fun EncryptedCredentials.toDto(): EncryptedCredentialsDto {
    return EncryptedCredentialsDto(
        data = data,
        hash = hash,
        secret = secret
    )
}