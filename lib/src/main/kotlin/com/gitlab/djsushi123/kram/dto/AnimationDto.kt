package com.gitlab.djsushi123.kram.dto

import com.gitlab.djsushi123.kram.entity.Animation
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
internal data class AnimationDto(
    @SerialName("file_id") val fileId: String,
    @SerialName("file_unique_id") val fileUniqueId: String,
    @SerialName("width") val width: Int,
    @SerialName("height") val height: Int,
    @SerialName("duration") val duration: Int,
    @SerialName("thumb") val thumb: PhotoSizeDto? = null,
    @SerialName("file_name") val fileName: String? = null,
    @SerialName("mime_type") val mimeType: String? = null,
    @SerialName("file_size") val fileSize: Long? = null
)

internal fun AnimationDto.toEntity(): Animation {
    return Animation(
        fileId = fileId,
        fileUniqueId = fileUniqueId,
        width = width,
        height = height,
        duration = duration,
        thumb = thumb?.toEntity(),
        fileName = fileName,
        mimeType = mimeType,
        fileSize = fileSize
    )
}

internal fun Animation.toDto(): AnimationDto {
    return AnimationDto(
        fileId = fileId,
        fileUniqueId = fileUniqueId,
        width = width,
        height = height,
        duration = duration,
        thumb = thumb?.toDto(),
        fileName = fileName,
        mimeType = mimeType,
        fileSize = fileSize
    )
}