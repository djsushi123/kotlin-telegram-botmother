package com.gitlab.djsushi123.kram.dto

import com.gitlab.djsushi123.kram.entity.Dice
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
internal data class DiceDto(
    @SerialName("emoji") val emoji: String,
    @SerialName("value") val value: Int
)

internal fun DiceDto.toEntity(): Dice {
    return Dice(
        emoji = emoji,
        value = value
    )
}

internal fun Dice.toDto(): DiceDto {
    return DiceDto(
        emoji = emoji,
        value = value
    )
}