package com.gitlab.djsushi123.kram.dto

import com.gitlab.djsushi123.kram.entity.VideoChatScheduled
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
internal data class VideoChatScheduledDto(
    @SerialName("start_date") val startDate: Int
)

internal fun VideoChatScheduledDto.toEntity(): VideoChatScheduled {
    return VideoChatScheduled(
        startDate = startDate
    )
}

internal fun VideoChatScheduled.toDto(): VideoChatScheduledDto {
    return VideoChatScheduledDto(
        startDate = startDate
    )
}