package com.gitlab.djsushi123.kram.dto

import com.gitlab.djsushi123.kram.entity.ChatInviteLink
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
internal data class ChatInviteLinkDto(
    @SerialName("invite_link") val inviteLink: String,
    @SerialName("creator") val creator: UserDto,
    @SerialName("creates_join_request") val createsJoinRequest: Boolean,
    @SerialName("is_primary") val isPrimary: Boolean,
    @SerialName("is_revoked") val isRevoked: Boolean,
    @SerialName("name") val name: String? = null,
    @SerialName("expire_date") val expireDate: Int? = null,
    @SerialName("member_limit") val memberLimit: Int? = null,
    @SerialName("pending_join_request_count") val pendingJoinRequestCount: Int? = null
)

internal fun ChatInviteLinkDto.toEntity(): ChatInviteLink {
    return ChatInviteLink(
        inviteLink = inviteLink,
        creator = creator.toEntity(),
        createsJoinRequest = createsJoinRequest,
        isPrimary = isPrimary,
        isRevoked = isRevoked,
        name = name,
        expireDate = expireDate,
        memberLimit = memberLimit,
        pendingJoinRequestCount = pendingJoinRequestCount
    )
}

internal fun ChatInviteLink.toDto(): ChatInviteLinkDto {
    return ChatInviteLinkDto(
        inviteLink = inviteLink,
        creator = creator.toDto(),
        createsJoinRequest = createsJoinRequest,
        isPrimary = isPrimary,
        isRevoked = isRevoked,
        name = name,
        expireDate = expireDate,
        memberLimit = memberLimit,
        pendingJoinRequestCount = pendingJoinRequestCount
    )
}