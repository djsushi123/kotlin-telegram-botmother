package com.gitlab.djsushi123.kram.dto

import com.gitlab.djsushi123.kram.entity.PollAnswer
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
internal data class PollAnswerDto(
    @SerialName("poll_id") val pollId: String,
    @SerialName("user") val user: UserDto,
    @SerialName("option_ids") val optionIds: List<Int>
)

internal fun PollAnswerDto.toEntity(): PollAnswer {
    return PollAnswer(
        pollId = pollId,
        user = user.toEntity(),
        optionIds = optionIds
    )
}

internal fun PollAnswer.toDto(): PollAnswerDto {
    return PollAnswerDto(
        pollId = pollId,
        user = user.toDto(),
        optionIds = optionIds
    )
}