package com.gitlab.djsushi123.kram.dto

import com.gitlab.djsushi123.kram.entity.Audio
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
internal data class AudioDto(
    @SerialName("file_id") val fileId: String,
    @SerialName("file_unique_id") val fileUniqueId: String,
    @SerialName("duration") val duration: Int,
    @SerialName("performer") val performer: String? = null,
    @SerialName("title") val title: String? = null,
    @SerialName("file_name") val fileName: String? = null,
    @SerialName("mime_type") val mimeType: String? = null,
    @SerialName("file_size") val fileSize: Long? = null,
    @SerialName("thumb") val thumb: PhotoSizeDto? = null
)

internal fun AudioDto.toEntity(): Audio {
    return Audio(
        fileId = fileId,
        fileUniqueId = fileUniqueId,
        duration = duration,
        performer = performer,
        title = title,
        fileName = fileName,
        mimeType = mimeType,
        fileSize = fileSize,
        thumb = thumb?.toEntity()
    )
}

internal fun Audio.toDto(): AudioDto {
    return AudioDto(
        fileId = fileId,
        fileUniqueId = fileUniqueId,
        duration = duration,
        performer = performer,
        title = title,
        fileName = fileName,
        mimeType = mimeType,
        fileSize = fileSize,
        thumb = thumb?.toDto()
    )
}