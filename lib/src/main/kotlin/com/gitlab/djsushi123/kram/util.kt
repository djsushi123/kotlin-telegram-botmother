package com.gitlab.djsushi123.kram

import kotlinx.serialization.json.encodeToJsonElement

internal fun jsonMapBodyOf(vararg pairs: Pair<String, Any?>) = pairs
    .toMap()
    .filter { it.value != null }
    .mapValues { Config.json.encodeToJsonElement(it.value) }