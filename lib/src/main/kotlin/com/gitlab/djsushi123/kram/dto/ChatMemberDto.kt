package com.gitlab.djsushi123.kram.dto

import com.gitlab.djsushi123.kram.entity.ChatMember
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
internal data class ChatMemberDto(
    @SerialName("status") val status: String,
    @SerialName("user") val user: UserDto,
    @SerialName("is_anonymous") val isAnonymous: Boolean? = null,
    @SerialName("custom_title") val customTitle: String? = null,
    @SerialName("can_be_edited") val canBeEdited: Boolean? = null,
    @SerialName("can_manage_chat") val canManageChat: Boolean? = null,
    @SerialName("can_delete_messages") val canDeleteMessages: Boolean? = null,
    @SerialName("can_manage_video_chats") val canManageVideoChats: Boolean? = null,
    @SerialName("can_restrict_members") val canRestrictMembers: Boolean? = null,
    @SerialName("can_promote_members") val canPromoteMembers: Boolean? = null,
    @SerialName("can_change_info") val canChangeInfo: Boolean? = null,
    @SerialName("can_invite_users") val canInviteUsers: Boolean? = null,
    @SerialName("can_post_messages") val canPostMessages: Boolean? = null,
    @SerialName("can_edit_messages") val canEditMessages: Boolean? = null,
    @SerialName("can_pin_messages") val canPinMessages: Boolean? = null,
    @SerialName("can_send_media_messages") val canSendMediaMessages: Boolean? = null,
    @SerialName("can_send_polls") val canSendPolls: Boolean? = null,
    @SerialName("can_send_other_messages") val canSendOtherMessages: Boolean? = null,
    @SerialName("can_add_web_page_previews") val canAddWebPagePreviews: Boolean? = null,
    @SerialName("until_date") val untilDate: Int? = null
)

internal fun ChatMemberDto.toEntity(): ChatMember {
    return ChatMember(
        status = status,
        user = user.toEntity(),
        isAnonymous = isAnonymous,
        customTitle = customTitle,
        canBeEdited = canBeEdited,
        canManageChat = canManageChat,
        canDeleteMessages = canDeleteMessages,
        canManageVideoChats = canManageVideoChats,
        canRestrictMembers = canRestrictMembers,
        canPromoteMembers = canPromoteMembers,
        canChangeInfo = canChangeInfo,
        canInviteUsers = canInviteUsers,
        canPostMessages = canPostMessages,
        canEditMessages = canEditMessages,
        canPinMessages = canPinMessages,
        canSendMediaMessages = canSendMediaMessages,
        canSendPolls = canSendPolls,
        canSendOtherMessages = canSendOtherMessages,
        canAddWebPagePreviews = canAddWebPagePreviews,
        untilDate = untilDate
    )
}

internal fun ChatMember.toDto(): ChatMemberDto {
    return ChatMemberDto(
        status = status,
        user = user.toDto(),
        isAnonymous = isAnonymous,
        customTitle = customTitle,
        canBeEdited = canBeEdited,
        canManageChat = canManageChat,
        canDeleteMessages = canDeleteMessages,
        canManageVideoChats = canManageVideoChats,
        canRestrictMembers = canRestrictMembers,
        canPromoteMembers = canPromoteMembers,
        canChangeInfo = canChangeInfo,
        canInviteUsers = canInviteUsers,
        canPostMessages = canPostMessages,
        canEditMessages = canEditMessages,
        canPinMessages = canPinMessages,
        canSendMediaMessages = canSendMediaMessages,
        canSendPolls = canSendPolls,
        canSendOtherMessages = canSendOtherMessages,
        canAddWebPagePreviews = canAddWebPagePreviews,
        untilDate = untilDate
    )
}