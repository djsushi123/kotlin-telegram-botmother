package com.gitlab.djsushi123.kram.entity

data class File(
    val fileId: String,
    val fileUniqueId: String,
    val fileName: String? = null,
    val fileSize: Long? = null
)