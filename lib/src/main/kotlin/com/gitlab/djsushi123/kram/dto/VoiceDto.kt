package com.gitlab.djsushi123.kram.dto

import com.gitlab.djsushi123.kram.entity.Voice
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
internal data class VoiceDto(
    @SerialName("file_id") val fileId: String,
    @SerialName("file_unique_id") val fileUniqueId: String,
    @SerialName("duration") val duration: Int,
    @SerialName("mime_type") val mimeType: String? = null,
    @SerialName("file_size") val fileSize: Long? = null
)

internal fun VoiceDto.toEntity(): Voice {
    return Voice(
        fileId = fileId,
        fileUniqueId = fileUniqueId,
        duration = duration,
        mimeType = mimeType,
        fileSize = fileSize
    )
}

internal fun Voice.toDto(): VoiceDto {
    return VoiceDto(
        fileId = fileId,
        fileUniqueId = fileUniqueId,
        duration = duration,
        mimeType = mimeType,
        fileSize = fileSize
    )
}