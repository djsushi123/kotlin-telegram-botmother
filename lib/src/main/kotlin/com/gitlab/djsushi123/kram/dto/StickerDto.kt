package com.gitlab.djsushi123.kram.dto

import com.gitlab.djsushi123.kram.entity.Sticker
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
internal data class StickerDto(
    @SerialName("file_id") val fileId: String,
    @SerialName("file_unique_id") val fileUniqueId: String,
    @SerialName("width") val width: Int,
    @SerialName("height") val height: Int,
    @SerialName("is_animated") val isAnimated: Boolean,
    @SerialName("is_video") val isVideo: Boolean,
    @SerialName("thumb") val thumb: PhotoSizeDto? = null,
    @SerialName("emoji") val emoji: String? = null,
    @SerialName("set_name") val setName: String? = null,
    @SerialName("premium_animation") val premiumAnimation: FileDto? = null,
    @SerialName("mask_position") val maskPosition: MaskPositionDto? = null,
    @SerialName("file_size") val fileSize: Long? = null
)

internal fun StickerDto.toEntity(): Sticker {
    return Sticker(
        fileId = fileId,
        fileUniqueId = fileUniqueId,
        width = width,
        height = height,
        isAnimated = isAnimated,
        isVideo = isVideo,
        thumb = thumb?.toEntity(),
        emoji = emoji,
        setName = setName,
        premiumAnimation = premiumAnimation?.toEntity(),
        maskPosition = maskPosition?.toEntity(),
        fileSize = fileSize
    )
}

internal fun Sticker.toDto(): StickerDto {
    return StickerDto(
        fileId = fileId,
        fileUniqueId = fileUniqueId,
        width = width,
        height = height,
        isAnimated = isAnimated,
        isVideo = isVideo,
        thumb = thumb?.toDto(),
        emoji = emoji,
        setName = setName,
        premiumAnimation = premiumAnimation?.toDto(),
        maskPosition = maskPosition?.toDto(),
        fileSize = fileSize
    )
}