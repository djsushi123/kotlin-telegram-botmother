package com.gitlab.djsushi123.kram.dto

import com.gitlab.djsushi123.kram.entity.OrderInfo
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
internal data class OrderInfoDto(
    @SerialName("name") val name: String? = null,
    @SerialName("phone_number") val phoneNumber: String? = null,
    @SerialName("email") val email: String? = null,
    @SerialName("shipping_address") val shippingAddress: ShippingAddressDto? = null
)

internal fun OrderInfoDto.toEntity(): OrderInfo {
    return OrderInfo(
        name = name,
        phoneNumber = phoneNumber,
        email = email,
        shippingAddress = shippingAddress?.toEntity()
    )
}

internal fun OrderInfo.toDto(): OrderInfoDto {
    return OrderInfoDto(
        name = name,
        phoneNumber = phoneNumber,
        email = email,
        shippingAddress = shippingAddress?.toDto()
    )
}