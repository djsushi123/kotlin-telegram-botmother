package com.gitlab.djsushi123.kram

import com.gitlab.djsushi123.kram.entity.MessageEntity
import com.gitlab.djsushi123.kram.entity.ParseMode
import com.gitlab.djsushi123.kram.entity.ReplyMarkup
import com.gitlab.djsushi123.kram.entity.Update
import com.gitlab.djsushi123.kram.entity.message.Message
import com.gitlab.djsushi123.kram.entity.message.TextMessage
import com.gitlab.djsushi123.kram.entity.message.toStickerMessage
import com.gitlab.djsushi123.kram.entity.message.toTextMessage
import com.gitlab.djsushi123.kram.event.Event
import com.gitlab.djsushi123.kram.event.MessageReceiveEvent
import com.gitlab.djsushi123.kram.event.StickerMessageReceiveEvent
import com.gitlab.djsushi123.kram.event.TextMessageReceiveEvent
import com.gitlab.djsushi123.kram.networking.ApiClient
import kotlinx.coroutines.*
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.flow.*
import kotlin.coroutines.CoroutineContext

class KramBot(
    val token: String,
    private val eventFlow: MutableSharedFlow<Event>
) : CoroutineScope {

    override val coroutineContext: CoroutineContext = SupervisorJob() + Dispatchers.Default

    private val apiClient = ApiClient.create(this)

    val events: SharedFlow<Event>
        get() = eventFlow

    suspend fun start() {
        apiClient.updateFlow.collect {
            handleUpdate(it)
        }
    }

    private suspend fun handleUpdate(update: Update) {
        if (update.message != null) {
            eventFlow.emit(MessageReceiveEvent(this, update.message))
            
            if (update.message.text != null) eventFlow.emit(TextMessageReceiveEvent(this, update.message.toTextMessage()))
            if (update.message.sticker != null) eventFlow.emit(StickerMessageReceiveEvent(this, update.message.toStickerMessage()))
        }
    }

    fun sendMessage(
        chatId: Int,
        text: String,
        parseMode: ParseMode? = null,
        entities: List<MessageEntity>? = null,
        disableWebPagePreview: Boolean? = null,
        disableNotification: Boolean? = null,
        protectContent: Boolean? = null,
        replyToMessageId: Int? = null,
        allowSendingWithoutReply: Boolean? = null,
        replyMarkup: ReplyMarkup? = null
    ) = launch {
        apiClient.sendMessage(
            chatId = chatId,
            text = text,
            parseMode = parseMode,
            entities = entities,
            disableWebPagePreview = disableWebPagePreview,
            disableNotification = disableNotification,
            protectContent = protectContent,
            replyToMessageId = replyToMessageId,
            allowSendingWithoutReply = allowSendingWithoutReply,
            replyMarkup = replyMarkup
        )
    }

    companion object {
        internal const val LONG_POLLING_TIMEOUT_SECONDS = 30
    }

}

inline fun <reified T : Event> KramBot.on(
    scope: CoroutineScope = this,
    noinline consumer: suspend T.() -> Unit
): Job = events
    .buffer(Channel.UNLIMITED)
    .filterIsInstance<T>()
    .onEach { event ->
        scope.launch(event.coroutineContext) { runCatching { consumer(event) }.onFailure { println("ERROR: $it") } }
    }
    .launchIn(scope)

fun kramBot(token: String) = KramBot(token = token, MutableSharedFlow())