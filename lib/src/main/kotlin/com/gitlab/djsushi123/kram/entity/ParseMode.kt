package com.gitlab.djsushi123.kram.entity

enum class ParseMode {
    MARKDOWN_V2,
    HTML,
    MARKDOWN_LEGACY
}