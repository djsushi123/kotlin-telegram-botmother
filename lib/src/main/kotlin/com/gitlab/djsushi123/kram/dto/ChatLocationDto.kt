package com.gitlab.djsushi123.kram.dto

import com.gitlab.djsushi123.kram.entity.ChatLocation
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
internal data class ChatLocationDto(
    @SerialName("location") val location: LocationDto,
    @SerialName("address") val address: String
)

internal fun ChatLocationDto.toEntity(): ChatLocation {
    return ChatLocation(
        location = location.toEntity(),
        address = address
    )
}

internal fun ChatLocation.toDto(): ChatLocationDto {
    return ChatLocationDto(
        location = location.toDto(),
        address = address
    )
}