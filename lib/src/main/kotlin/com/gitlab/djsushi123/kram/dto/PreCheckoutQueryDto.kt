package com.gitlab.djsushi123.kram.dto

import com.gitlab.djsushi123.kram.entity.PreCheckoutQuery
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
internal data class PreCheckoutQueryDto(
    @SerialName("id") val id: String,
    @SerialName("from") val from: UserDto,
    @SerialName("currency") val currency: String,
    @SerialName("total_amount") val totalAmount: Int,
    @SerialName("invoice_payload") val invoicePayload: String,
    @SerialName("shipping_option_id") val shippingOptionId: String? = null,
    @SerialName("order_info") val orderInfo: OrderInfoDto? = null
)

internal fun PreCheckoutQueryDto.toEntity(): PreCheckoutQuery {
    return PreCheckoutQuery(
        id = id,
        from = from.toEntity(),
        currency = currency,
        totalAmount = totalAmount,
        invoicePayload = invoicePayload,
        shippingOptionId = shippingOptionId,
        orderInfo = orderInfo?.toEntity()
    )
}

internal fun PreCheckoutQuery.toDto(): PreCheckoutQueryDto {
    return PreCheckoutQueryDto(
        id = id,
        from = from.toDto(),
        currency = currency,
        totalAmount = totalAmount,
        invoicePayload = invoicePayload,
        shippingOptionId = shippingOptionId,
        orderInfo = orderInfo?.toDto()
    )
}