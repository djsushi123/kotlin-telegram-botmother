package com.gitlab.djsushi123.kram.dto

import com.gitlab.djsushi123.kram.entity.MessageEntity
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
internal data class MessageEntityDto(
    @SerialName("type") val type: String,
    @SerialName("offset") val offset: Int,
    @SerialName("length") val length: Int,
    @SerialName("url") val url: String? = null,
    @SerialName("user") val user: UserDto? = null,
    @SerialName("language") val language: String? = null
)

internal fun MessageEntityDto.toEntity() = MessageEntity(
    type = type,
    offset = offset,
    length = length,
    url = url
)

internal fun MessageEntity.toDto() = MessageEntityDto(
    type = type,
    offset = offset,
    length = length,
    url = url
)