package com.gitlab.djsushi123.kram.dto

import com.gitlab.djsushi123.kram.entity.Invoice
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
internal data class InvoiceDto(
    @SerialName("title") val title: String,
    @SerialName("description") val description: String,
    @SerialName("start_parameter") val startParameter: String,
    @SerialName("currency") val currency: String,
    @SerialName("total_amount") val totalAmount: Int,
)

internal fun InvoiceDto.toEntity(): Invoice {
    return Invoice(
        title = title,
        description = description,
        startParameter = startParameter,
        currency = currency,
        totalAmount = totalAmount
    )
}

internal fun Invoice.toDto(): InvoiceDto {
    return InvoiceDto(
        title = title,
        description = description,
        startParameter = startParameter,
        currency = currency,
        totalAmount = totalAmount
    )
}