package com.gitlab.djsushi123.kram.networking

import com.gitlab.djsushi123.kram.Config
import com.gitlab.djsushi123.kram.KramBot
import com.gitlab.djsushi123.kram.dto.*
import com.gitlab.djsushi123.kram.entity.MessageEntity
import com.gitlab.djsushi123.kram.entity.ParseMode
import com.gitlab.djsushi123.kram.entity.ReplyMarkup
import com.gitlab.djsushi123.kram.entity.Update
import io.ktor.client.*
import io.ktor.client.call.*
import io.ktor.client.engine.cio.*
import io.ktor.client.plugins.*
import io.ktor.client.plugins.contentnegotiation.*
import io.ktor.client.plugins.logging.*
import io.ktor.client.request.*
import io.ktor.http.*
import io.ktor.serialization.kotlinx.json.*
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

internal class ApiClient private constructor(private val bot: KramBot) {

    private val client = HttpClient(CIO) {
        install(DefaultRequest)
        install(ContentNegotiation) {
            json(Config.json)
        }
        install(HttpTimeout) {
            connectTimeoutMillis = 30_000
            requestTimeoutMillis = 30_000
        }
        install(Logging) {
            level = LogLevel.ALL
//            level = LogLevel.NONE
        }
        defaultRequest {
            url("https://api.telegram.org/bot${bot.token}/")
        }
    }

    private var currentOffset = 0

    internal val updateFlow: Flow<Update> = flow {
        while (true) {
            try {
                val updates = getUpdates(
                    offset = currentOffset,
                    timeout = KramBot.LONG_POLLING_TIMEOUT_SECONDS
                )
                // Telegram servers return an empty JSON array after the timeout if there are no updates
                if (updates.isEmpty()) continue
                updates.forEach { emit(it) }
                currentOffset = updates.last().updateId + 1
            } catch (e: HttpRequestTimeoutException) {
                println("Timed out.")
            }
        }
    }

    private suspend fun getUpdates(
        offset: Int? = null,
        limit: Int? = null,
        timeout: Int? = null,
        allowedUpdates: List<String>? = null
    ): List<Update> {
        val response = client.post("getUpdates") {
            with(url.parameters) {
                offset?.let { append("offset", it.toString()) }
                limit?.let { append("limit", it.toString()) }
                timeout?.let { append("timeout", it.toString()) }
                allowedUpdates?.let { append("allowed_updates", it.toString()) }
            }
        }
        when (val apiResponse = response.body<ApiResponse<List<UpdateDto>>>()) {
            is ApiResponse.Ok -> return apiResponse.result.map { it.toEntity() }
            is ApiResponse.Error -> error(
                """
                    Got an error response from the getUpdates method which should be impossible.
                    Error code: ${apiResponse.errorCode}
                    Description: ${apiResponse.description}
                """.trimIndent()
            )
        }
    }

    internal suspend fun sendMessage(
        chatId: Int,
        text: String,
        parseMode: ParseMode? = null,
        entities: List<MessageEntity>? = null,
        disableWebPagePreview: Boolean? = null,
        disableNotification: Boolean? = null,
        protectContent: Boolean? = null,
        replyToMessageId: Int? = null,
        allowSendingWithoutReply: Boolean? = null,
        replyMarkup: ReplyMarkup? = null
    ) {
        @Serializable
        class Request(
            @SerialName("chat_id") val chatId: Int,
            @SerialName("text") val text: String,
            @SerialName("parse_mode") val parseMode: ParseModeDto? = null,
            @SerialName("entities") val entities: List<MessageEntityDto>? = null,
            @SerialName("disable_web_page_preview") val disableWebPagePreview: Boolean? = null,
            @SerialName("disable_notification") val disableNotification: Boolean? = null,
            @SerialName("protect_content") val protectContent: Boolean? = null,
            @SerialName("reply_to_message_id") val replyToMessageId: Int? = null,
            @SerialName("allow_sending_without_reply") val allowSendingWithoutReply: Boolean? = null,
            @SerialName("reply_markup") val replyMarkup: ReplyMarkup? = null
        )
        client.post("sendMessage") {
            contentType(ContentType.Application.Json)
            setBody(
                Request(
                    chatId = chatId,
                    text = text,
                    parseMode = parseMode?.toDto(),
                    entities = entities?.map { it.toDto() },
                    disableWebPagePreview = disableWebPagePreview,
                    disableNotification = disableNotification,
                    protectContent = protectContent,
                    replyToMessageId = replyToMessageId,
                    allowSendingWithoutReply = allowSendingWithoutReply,
                    replyMarkup = replyMarkup
                )
            )
        }
    }

    companion object {

        fun create(bot: KramBot) = ApiClient(bot)

    }
}