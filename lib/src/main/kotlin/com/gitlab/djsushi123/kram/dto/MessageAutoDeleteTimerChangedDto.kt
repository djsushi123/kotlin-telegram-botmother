package com.gitlab.djsushi123.kram.dto

import com.gitlab.djsushi123.kram.entity.MessageAutoDeleteTimerChanged
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
internal data class MessageAutoDeleteTimerChangedDto(
    @SerialName("message_auto_delete_time") val messageAutoDeleteTime: Int
)

internal fun MessageAutoDeleteTimerChangedDto.toEntity(): MessageAutoDeleteTimerChanged {
    return MessageAutoDeleteTimerChanged(
        messageAutoDeleteTime = messageAutoDeleteTime
    )
}

internal fun MessageAutoDeleteTimerChanged.toDto(): MessageAutoDeleteTimerChangedDto {
    return MessageAutoDeleteTimerChangedDto(
        messageAutoDeleteTime = messageAutoDeleteTime
    )
}