package com.gitlab.djsushi123.kram.dto

import com.gitlab.djsushi123.kram.entity.InlineKeyboardMarkup
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
internal data class InlineKeyboardMarkupDto(
    @SerialName("inline_keyboard") val inlineKeyboard: List<List<InlineKeyboardButtonDto>>
)

internal fun InlineKeyboardMarkupDto.toEntity(): InlineKeyboardMarkup {
    return InlineKeyboardMarkup(
        inlineKeyboard = inlineKeyboard.map { keyboardButtonList ->
            keyboardButtonList.map { it.toEntity() }
        }
    )
}

internal fun InlineKeyboardMarkup.toDto(): InlineKeyboardMarkupDto {
    return InlineKeyboardMarkupDto(
        inlineKeyboard = inlineKeyboard.map { keyboardButtonList ->
            keyboardButtonList.map { it.toDto() }
        }
    )
}