package com.gitlab.djsushi123.kram.dto

import com.gitlab.djsushi123.kram.entity.MaskPosition
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
internal data class MaskPositionDto(
    @SerialName("point") val point: String,
    @SerialName("x_shift") val xShift: Float,
    @SerialName("y_shift") val yShift: Float,
    @SerialName("scale") val scale: Float
)

internal fun MaskPositionDto.toEntity(): MaskPosition {
    return MaskPosition(
        point = point,
        xShift = xShift,
        yShift = yShift,
        scale = scale
    )
}

internal fun MaskPosition.toDto(): MaskPositionDto {
    return MaskPositionDto(
        point = point,
        xShift = xShift,
        yShift = yShift,
        scale = scale
    )
}