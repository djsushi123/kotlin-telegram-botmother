package com.gitlab.djsushi123.kram.entity

data class ChosenInlineResult(
    val resultId: String,
    val from: User,
    val location: Location? = null,
    val inlineMessageId: String? = null,
    val query: String
)