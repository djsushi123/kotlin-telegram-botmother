package com.gitlab.djsushi123.kram.dto

import com.gitlab.djsushi123.kram.entity.Contact
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
internal data class ContactDto(
    @SerialName("phone_number") val phoneNumber: String,
    @SerialName("first_name") val firstName: String,
    @SerialName("last_name") val lastName: String? = null,
    @SerialName("user_id") val userId: Long? = null,
    @SerialName("vcard") val vcard: String? = null
)

internal fun ContactDto.toEntity(): Contact {
    return Contact(
        phoneNumber = phoneNumber,
        firstName = firstName,
        lastName = lastName,
        userId = userId,
        vcard = vcard
    )
}

internal fun Contact.toDto(): ContactDto {
    return ContactDto(
        phoneNumber = phoneNumber,
        firstName = firstName,
        lastName = lastName,
        userId = userId,
        vcard = vcard
    )
}