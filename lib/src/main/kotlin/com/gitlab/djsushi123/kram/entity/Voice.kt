package com.gitlab.djsushi123.kram.entity

data class Voice(
    val fileId: String,
    val fileUniqueId: String,
    val duration: Int,
    val mimeType: String? = null,
    val fileSize: Long? = null
)