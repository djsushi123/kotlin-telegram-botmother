package com.gitlab.djsushi123.kram.entity

data class Dice(
    val emoji: String,
    val value: Int
)