package com.gitlab.djsushi123.kram.dto

import com.gitlab.djsushi123.kram.entity.ReplyKeyboardRemove
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class ReplyKeyboardRemoveDto(
    @SerialName("remove_keyboard") val removeKeyboard: Boolean,
    @SerialName("selective") val selective: Boolean? = null
)

internal fun ReplyKeyboardRemoveDto.toEntity(): ReplyKeyboardRemove {
    return ReplyKeyboardRemove(
        removeKeyboard = removeKeyboard,
        selective = selective
    )
}

internal fun ReplyKeyboardRemove.toDto(): ReplyKeyboardRemoveDto {
    return ReplyKeyboardRemoveDto(
        removeKeyboard = removeKeyboard,
        selective = selective
    )
}